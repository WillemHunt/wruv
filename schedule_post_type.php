function create_posttype() {
	
//Set ui labels for post type
	$labels = array(
		'name' => __('Shows'),
		'singular_name' => __('Show'),
		'menu_name' => __('Shows'),
		'add_new_item' => __('Add new show'),
		'add_new' => __('Add new'),
		'edit_item' => __('Edit show'),
		'update_item' => __('Update show')
	);
	
// other post type options
	$args = array(
		'label' => __('shows'),
		'description' => __('Schedule items'),
		'labels' => $labels,
		'supports' => array('title','editor','revisions'),
		'taxonomies' => array('genres'),  //not sure about this...  will look into it further
		'hierarchical' => false,
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => false,
		'show_in_admin_bar' => true,
		'menu_position' => 5,
		'can_export' => true,
		'has_archive' => true,
		'exclude_from_search' => false,
		'publicly_queryable' => true,
		'capability_type' => 'page'
	);
	
//register the post type
	register_post_type('schedule_item',$args);

}

function show_update_messages( $messages ) {
  global $post, $post_ID;
  $messages['show'] = array(
    0 => '', 
    1 => sprintf( __('Show updated. <a href="%s">View show</a>'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field deleted.'),
    4 => __('Show updated.'),
    5 => isset($_GET['revision']) ? sprintf( __('Show restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Show published. <a href="%s">View show</a>'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Show saved.'),
    8 => sprintf( __('Show submitted. <a target="_blank" href="%s">Preview show</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Show scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview show</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Show draft updated. <a target="_blank" href="%s">Preview show</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );
  return $messages;
}
add_filter( 'post_updated_messages', 'show_update_messages' );

function custom_taxonomies_show() {
  $labels = array(
    'name'              => _x( 'Show Categories', 'taxonomy general name' ),
    'singular_name'     => _x( 'Show Category', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Show Categories' ),
    'all_items'         => __( 'All Show Categories' ),
    'parent_item'       => __( 'Parent Show Category' ),
    'parent_item_colon' => __( 'Parent Show Category:' ),
    'edit_item'         => __( 'Edit Show Category' ), 
    'update_item'       => __( 'Update Show Category' ),
    'add_new_item'      => __( 'Add New Show Category' ),
    'new_item_name'     => __( 'New Show Category' ),
    'menu_name'         => __( 'Show Categories' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => true,
  );
  register_taxonomy( 'show_category', 'Show', $args );
}
add_action( 'init', 'custom_taxonomies_show', 0 );